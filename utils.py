import time
import os
import numpy as np
import pandas as pd
from skimage import io
import cv2

def get_execution_time(start, text =''):
    '''
    return execution time starting from start
    '''
    part = time.time()
    hours, rem = divmod(part-start, 3600)
    minutes, seconds = divmod(rem, 60)
    duration = '{:0>2}:{:0>2}:{:05.2f}\n'.format(int(hours),int(minutes),seconds)
    print('{}: {}'.format(text, duration))

    return duration

def load_data(X_path, label_path=''):
    '''
    load X set and y labels
    '''
    # Import X_set
    X = np.array([io.imread(os.path.join(X_path, file)) for file in np.sort(os.listdir(X_path))])

    if label_path != '':
        # Import labels
        y = pd.read_csv(label_path, dtype=int, names=['image_index', 'i', 'j', 'height', 'width'], delimiter=' ')
        return X, y

    return X


def show_windows_on_image(image, predictions):
    '''
    show predictions on image
    '''
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
  
    if predictions == []:
        cv2.imshow("predictions", image)

    for window in predictions:
        if len(window) > 4:
            cv2.rectangle(image, (window[2],window[1]), (window[2] + window[4], window[1] + window[3]), (0, 255, 127),2)
            cv2.putText(image, str(window[5]), (window[2], window[1]+12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 127), 1)

    cv2.imshow("predictions", image)
    cv2.waitKey(1)

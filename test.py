'''
Test script
'''

from face_detector import face_detector
from utils import load_data
import argparse

#Hyperparameters
parser = argparse.ArgumentParser()

parser.add_argument('--window_height', type=int , default=64,
                    help='height of the sliding window, must be the same for the train and test file  [default: 64]')
parser.add_argument('--window_width', type=int , default=64,
                    help='width of the sliding window, must be the same for the train and test file  [default: 64]')
parser.add_argument('--image_layers', type=int, default=10,
                    help='number of image layers to slide the window over [default: 10]')
parser.add_argument('--step', type=int, default=6,
                    help='step of the sliding window [default: 6]')
parser.add_argument('--test_path', type=str, default='test/',
                    help='path to the folder containing test images [default: test/ ]')
parser.add_argument('--predictions_path', type=str, default='detection.txt',
                    help='file path to store the predictions [default: detection.txt]')

FLAGS = parser.parse_args()

#WINDOW_SHAPE must be the same as in the train file
WINDOW_SHAPE = (FLAGS.window_height, FLAGS.window_width)
IMAGE_LAYERS = FLAGS.image_layers
STEP = FLAGS.step
TEST_PATH = FLAGS.test_path
PREDICTIONS_PATH = FLAGS.predictions_path

X = load_data(TEST_PATH)

detector = face_detector(window_shape=WINDOW_SHAPE, image_layers=IMAGE_LAYERS, step=STEP)
detector.predict(X, predictions_path=PREDICTIONS_PATH)

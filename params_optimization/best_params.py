import sys
sys.path.append("..")

from face_detector import face_detector, np, io, os, pd, MinMaxScaler, StandardScaler
from utils import get_execution_time, time, load_data
from multiprocessing import Pool
from sklearn.model_selection import KFold
import argparse

# pylint: disable=unsubscriptable-object # pylint/issues/3139
#Parameters
start = time.time()

parser = argparse.ArgumentParser()

parser.add_argument('--train_path', type=str, default='../train/',
                    help='path to the folder containing train images [default: ../train/ ]')
parser.add_argument('--label_path', type=str, default='../label_train.txt',
                    help='path to the labels file [default: ../label_train.txt]')
parser.add_argument('--predictions_path', type=str, default='detection.txt',
                    help='file path to store the predictions [default: detection.txt]')

FLAGS = parser.parse_args()


TRAIN_PATH = FLAGS.train_path
LABEL_PATH = FLAGS.label_path
PREDICTIONS_PATH = FLAGS.predictions_path


if os.path.exists(PREDICTIONS_PATH):
    os.remove(PREDICTIONS_PATH)

    
def cv_test(split):

    train_index, test_index = split[0], split[1]

    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[y.image_index.isin([index+1 for index in train_index])], y[y.image_index.isin([index+1 for index in test_index])]
    
    MinMax_scaler = MinMaxScaler()
    Standard_scaler = StandardScaler()

    clf_path='classifier_'+str(split[1][0])+'.joblib'
    detector = face_detector(window_shape=(64, 64), image_layers=4, step=15)
    detector.fit(X_train, y_train, train_path=TRAIN_PATH, clf_path=clf_path, symetric=False, rotation=True)

    detector = face_detector(window_shape=(64, 64), image_layers=4, step=15)
    detector.predict(X_test, y_test, predictions_path=PREDICTIONS_PATH, append_predictions=True, clf_path=clf_path)

X, y = load_data(TRAIN_PATH, LABEL_PATH)

kf = KFold(n_splits=3)
cv_index = [split for split in kf.split(X)]

with Pool(processes=3) as p:
    p.map(cv_test, cv_index)


detector = face_detector()

predictions = pd.read_csv(PREDICTIONS_PATH, names=['image_index', 'i', 'j', 'height','width','score'], delimiter =' ', index_col= False)
predictions, nbr_faces_detected = detector._get_true_predictions(predictions, y)
F1, AUC, Precision, Recall = detector._get_metrics(predictions, y, nbr_faces_detected)

with open(PREDICTIONS_PATH, 'a+') as f:
    f.write(get_execution_time(start, 'Total execution'))
    f.write(f'Precision: {Precision}\nRecall: {Recall}\nF1: {F1}\nAUC: {AUC}')

# os.system('shutdown')


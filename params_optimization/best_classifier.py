from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from multiprocessing import Pool
from skimage.feature import hog
from itertools import product
import json
import sys
import argparse

sys.path.append("..")
from utils import get_execution_time, load_data
from face_detector import face_detector, np, time, io, os, pd, plt, tqdm
# pylint: disable=unsubscriptable-object # pylint/issues/3139
start = time.time()

def show_scores(clf_scores):
    '''
    draw a boxplot
    '''
    print(clf_scores)
    plt.boxplot(x=clf_scores.values(), labels=clf_scores.keys() )
    plt.show()


def fit_and_get_score(clf, X_train, X_test, Y_train, Y_test):
    '''
    fit a classifier and return the test score
    '''
    clf.fit(X_train, Y_train)
    print('    Train score: ', clf.score(X_train, Y_train))
    print('    Test score: ', clf.score(X_test, Y_test))
    return clf.score(X_test, Y_test)

def best_classifier(X_clf_features, Y_clf):
    '''
    fit and get scores of different classifers
    '''
    classifiers = [SVC(),\
                GaussianNB(),\
                LogisticRegression(max_iter=1000),\
                LinearSVC(),\
                AdaBoostClassifier(),\
                KNeighborsClassifier(),\
                DecisionTreeClassifier(),\
                RandomForestClassifier()
                ]
    sss = StratifiedShuffleSplit(n_splits=3)
    clf_scores = {}

    for i, clf in enumerate(tqdm(classifiers)):
        start = time.time()
        test_scores = []
        print(type(clf).__name__)
    
        for train_index, test_index in  tqdm(sss.split(X_clf_features, Y_clf)):
            X_train, X_test = X_clf_features[train_index], X_clf_features[test_index]
            Y_train, Y_test = Y_clf[train_index], Y_clf[test_index]
            test_scores.append(fit_and_get_score(clf, X_train, X_test, Y_train, Y_test))
    
        duration = get_execution_time(start)
        clf_scores[type(clf).__name__ + '\n' + duration] = test_scores
        print(duration)
        print(clf_scores)
    show_scores(clf_scores)

def tune_hyperparameters(X_clf_features, Y_clf, model, hyperparameters):
    '''
    perform a grid search CV on different hyperparameters
    '''
    sss = StratifiedShuffleSplit(n_splits=5)
    cv = sss.split(X_clf_features, Y_clf)
    
    clf = GridSearchCV(model, hyperparameters,  cv=sss, verbose=20)
    clf.fit(X_clf_features, Y_clf)

    print('best params',clf.best_params_)
    print('best score',clf.best_score_)
    print('results',clf.cv_results_)
    with open('best_hyperparameters', 'w') as f:
        f.write(clf.best_params_)
        f.write(clf.best_score_)
        f.write(clf.cv_results_)
        
def best_hog(X, y):
    '''
    return classifier score with different HOG parameters
    '''
    orientations = [9, 12]
    pixels_per_cell=[(4, 4), (8, 8)]
    cells_per_block=[(2, 2), (3, 3), (4, 4)]
    block_norm=['L2', 'L1-sqrt']

    params = [orientations, pixels_per_cell, cells_per_block, block_norm]
    hog_params = list(product(*params))
    clf = SVC(C=10, kernel='poly', degree=4, gamma='scale')
    
    clf_scores = {}

    for params in tqdm(hog_params):
        start = time.time()
        print(params)
        
        detector = face_detector()
        detector._change_labels_to_square(y, TRAIN_PATH)

        detector.orientations = params[0]
        detector.pixels_per_cell = params[1]
        detector.cells_per_block = params[2]
        detector.block_norm = params[3]

        X_clf_positive = detector._get_positive_images(X, y, symetric=True, rotation=True)
        X_clf_negative = detector._generate_negative_image_examples(X, y)

        Y_clf = detector._get_image_labels(X_clf_positive, X_clf_negative)
        X_clf = np.concatenate((X_clf_positive, X_clf_negative))
        del X_clf_positive, X_clf_negative
        X_clf = detector._preprocess(X_clf)

        sss = StratifiedShuffleSplit(n_splits=3)
        cv_index = [split for split in sss.split(X_clf, Y_clf)]
        test_scores = []

        for train_index, test_index in  tqdm(sss.split(X_clf, Y_clf)):
            X_train, X_test = X_clf[train_index], X_clf[test_index]
            Y_train, Y_test = Y_clf[train_index], Y_clf[test_index]
            clf.fit(X_train, Y_train)
            score = clf.score(X_test, Y_test)
            print(score)
            test_scores.append(score)
        
        duration = get_execution_time(start)
        
        clf_scores['orientations: '+ str(params[0]) + '\n'\
                + 'pixels per cell: '+ str(params[1]) + '\n'\
                + 'cells per block: '+ str(params[2]) + '\n'\
                + 'block norm: '+ str(params[3]) + '\n' + duration] = test_scores
 
        print(test_scores, duration)
    
    show_scores(clf_scores)
    

    
parser = argparse.ArgumentParser()

parser.add_argument('--window_height', type=int , default=64,
                    help='height of the sliding window, must be the same for the train and test file  [default: 64]')
parser.add_argument('--window_width', type=int , default=64,
                    help='width of the sliding window, must be the same for the train and test file  [default: 64]')
parser.add_argument('--image_layers', type=int, default=10,
                    help='number of image layers to slide the window over [default: 10]')
parser.add_argument('--step', type=int, default=6,
                    help='step of the sliding window [default: 6]')
parser.add_argument('--train_path', type=str, default='../train/',
                    help='path to the folder containing train images [default: ../train/ ]')
parser.add_argument('--label_path', type=str, default='../label_train.txt',
                    help='path to the labels file [default: ../label_train.txt]')

FLAGS = parser.parse_args()

WINDOW_SHAPE = (FLAGS.window_height, FLAGS.window_width)
IMAGE_LAYERS = FLAGS.image_layers
STEP = FLAGS.step
TRAIN_PATH = FLAGS.train_path
LABEL_PATH = FLAGS.label_path




X, y = load_data(TRAIN_PATH, LABEL_PATH)

# Uncomment for best hog
best_hog(X, y)

#First classifier
detector = face_detector(window_shape=WINDOW_SHAPE, image_layers=IMAGE_LAYERS, step=STEP)
detector._change_labels_to_square(y, TRAIN_PATH)

# Generate positive and negative image examples
X_clf_positive = detector._get_positive_images(X, y, symetric=True, rotation=False)
X_clf_negative = detector._generate_negative_image_examples(X, y)
 
Y_clf = detector._get_image_labels(X_clf_positive, X_clf_negative)
X_clf = np.concatenate((X_clf_positive, X_clf_negative))
del X_clf_positive, X_clf_negative
X_clf = detector._preprocess(X_clf)

# Uncomment for best classifier
# best_classifier(X_clf, Y_clf)

#Uncomment for tunining hyperparameters
model =  SVC()
hyperparameters = {'kernel':('linear', 'rbf', 'poly', 'sigmoid'), 
                    'C':np.logspace(0,2,3), 
                    'degree':[2,3,4], 
                    'gamma':('scale','auto')}
tune_hyperparameters(X_clf, Y_clf, model, hyperparameters)


# Training first classifier
clf = SVC(C=10, kernel='poly', degree=4, gamma='scale')
clf.fit(X_clf, Y_clf)

#Second classifier
# Generate False positives
false_positives = detector._get_false_positives(X, y, clf)
if false_positives.shape[0] != 0:
    X_clf = np.concatenate((X_clf, false_positives))
    X_clf = detector._preprocess(X_clf)
    Y_clf = np.append(Y_clf, [-1] * (X_clf.shape[0] - Y_clf.shape[0]))


# Uncomment for best classifier
# best_classifier(X_clf, Y_clf)

#Uncomment for tunining hyperparameters
# model =  SVC()
# hyperparameters = {'kernel':('linear', 'rbf', 'poly', 'sigmoid'), 
#                     'C':np.logspace(0,2,3), 
#                     'degree':[2,3,4], 
#                     'gamma':('scale','auto')}
# tune_hyperparameters(X_clf, Y_clf, model, hyperparameters)
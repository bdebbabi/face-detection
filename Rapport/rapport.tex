\documentclass[a4paper, 12pt, french]{article}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[section]{placeins}
\usepackage{appendix}
\usepackage{listings}
\usepackage{titlesec}
\usepackage{subcaption}
\usepackage{makecell}
\usepackage{adjustbox}
\usepackage{array}

\newcolumntype{?}{!{\vrule width 1pt}}
\titleclass{\subsubsubsection}{straight}[\subsection]

\newcounter{subsubsubsection}[subsubsection]
\renewcommand\thesubsubsubsection{\thesubsubsection.\arabic{subsubsubsection}}
\renewcommand\theparagraph{\thesubsubsubsection.\arabic{paragraph}} % optional; useful if paragraphs are to be numbered

\titleformat{\subsubsubsection}
{\normalfont\normalsize\bfseries}{\thesubsubsubsection}{1em}{}
\titlespacing*{\subsubsubsection}
{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\makeatletter
\renewcommand\paragraph{\@startsection{paragraph}{5}{\z@}%
{3.25ex \@plus1ex \@minus.2ex}%
{-1em}%
{\normalfont\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{6}{\parindent}%
{3.25ex \@plus1ex \@minus .2ex}%
{-1em}%
{\normalfont\normalsize\bfseries}}
\def\toclevel@subsubsubsection{4}
\def\toclevel@paragraph{5}
\def\toclevel@paragraph{6}
\def\l@subsubsubsection{\@dottedtocline{4}{7em}{4em}}
\def\l@paragraph{\@dottedtocline{5}{10em}{5em}}
\def\l@subparagraph{\@dottedtocline{6}{14em}{6em}}
\makeatother

\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

\date{\vspace{-5ex}}

\title{Projet SY32 - Détection de visage}
\author{DEBBABI Bilel}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Sommaire}
\tableofcontents
\clearpage
\section{Introduction}
Le but de ce projet est de créer un détecteur de visages, en utilisant une fenêtre glissante.

\vspace{5mm} 
Il faut exécuter le fichier \path{train.py}, afin d'entraîner le classifieur, 
et le fichier \path{test.py}, afin de faire les prédictions. 
Il est possible de spécifier certains arguments lors de l’exécution de ces deux fichiers.
Pour voir la liste des arguments et leurs valeurs par défaut, il suffit de taper:
\begin{lstlisting}[language=bash]
  python train.py -h
\end{lstlisting}

Les arguments \texttt{window\_height} et \texttt{window\_width} doivent être les mêmes
entre le train et le test. 
Les prédictions sont enregistrées par défaut dans le fichier \path{detection.txt}.
Le classifieur obtenu à la fin de l'entraînement est enregistré dans le fichier \texttt{classifier.joblib}.
L'utilisation du prédicteur se fait avec la classe \texttt{face\_detector} définie dans le fichier \path{face_detector.py}.  

\vspace{5mm} 
Les modules suivants sont utilisés dans le code:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item \texttt{json}
  \item \texttt{sys}
  \item \texttt{time}
  \item \texttt{itertools}
  \item \texttt{random}
  \item \texttt{math}  
  \item \texttt{tqdm}  
  \item \texttt{joblib}
  \item \texttt{pandas} 
  \item \texttt{numpy} 
  \item \texttt{matplotlib}
  \item \texttt{cv2}
  \item \texttt{skimage}
  \item \texttt{sklearn}
\end{itemize}

\section{Classifieur}
Afin de réaliser la détection de visages avec une fenêtre glissante, il faut classer chaque image récupérée par la fenêtre,
et dire si elle représente un visage ou pas.
Il faut donc créer un classifieur de visages.

\vspace{5mm} 
Ce classifieur sera créé en deux étapes d'apprentissages. 
Afin de créer ce classifieur, on appelle la méthode \texttt{fit}.
\subsection{Premier apprentissage}
\subsubsection{Génération des exemples}
Pour entraîner le classifieur il faut générer des exemples positifs et négatifs de visages.
La fenêtre glissante du détecteur a une taille fixe (64x64). Il faut donc choisir cette même taille pour les données d'apprentissage du classifieur.

\subsubsubsection{Exemples positifs}
On récupère les images en utilisant les labels donnés dans le fichier \path{label_train.txt}.
Cela est fait avec la méthode \texttt{\_get\_positive\_images}. 
La plupart des images sont sous la forme d'un rectangle, il faut donc les transformer en carré.
Pour cela, il faut prendre soit le carré à l'intérieur du rectangle, soit celui à l’extérieur.
\begin{figure}[ht!]
   \centering
   \includegraphics[width=.17\linewidth]{../images/original_window.png}
   \caption{Image originale du visage}
   \label{}
\end{figure}

Si on prend celui à l'intérieur, on risque de perdre des parties importantes du visages comme on peut le voir dans les images ci dessous.

\begin{figure}[ht!]
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=.4\linewidth]{../images/resized_small_bottom.png}
      \caption{Image en bas du visage}
      \label{fig:sub1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=.4\linewidth]{../images/resized_small_centered.png}
      \caption{Image centrée sur le visage}
      \label{fig:sub2}
    \end{subfigure}
    \caption{Image du visage à l'intérieur du rectangle}
    \label{fig:test}
\end{figure}
Il est donc préférable de prendre l'image avec le carré à l'extérieur du rectangle. 
De cette manière on peut avoir un peu plus de contexte autour du visage. 
Par contre, il faut faire attention, quand le rectangle est à coté d'un bord de l'image d'origine, on risque de dépasser ce bord. 
\\Afin d'obtenir la taille souhaitée on peut soit décaler le carré, soit étirer le rectangle collé au bord. Si on étire le rectangle on perd la qualité de l'image. 
Il est donc préférable de décaler le carré, au risque d'avoir le visage qui ne sera pas centré dans le carré.
Cela est fait avec la méthode \texttt{\_change\_labels\_to\_square}. 

\begin{figure}[ht!]
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=.4\linewidth]{../images/resized_big_without_border_correction.png}
      \caption{Image étirée}
      \label{fig:sub1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=.4\linewidth]{../images/resized_big.png}
      \caption{Image décalée}
      \label{fig:sub2}
    \end{subfigure}
    \caption{Image du visage à l'extérieur du rectangle}
    \label{fig:test}
\end{figure}


Afin d'améliorer les performances du classifieur, on peut augmenter le nombre d'exemples
positifs en ajoutant des symétries et des rotations.
Pour les rotations il faut déterminer l'angle adéquat. 
Dans la figure \ref{Score_du_classifieur_avec_différentes_rotations}, je compare
les scores du classifieur sans rotation et avec différentes rotations.
J'utilise une validation croisée \texttt{StratifiedShuffleSplit} à 3 splits avec un \texttt{SVC}.
\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=1.3\linewidth]{../images/rotation_scores.png}}
  \caption{Score du classifieur avec différentes rotations}
  \label{Score_du_classifieur_avec_différentes_rotations}
\end{figure}
\clearpage
\vspace{5mm} 
On obtient les meilleurs scores avec des angles de rotation faibles. 
Le temps d’exécution augmente également avec le nombre d'angles. 
On remarque qu'en moyenne les meilleurs scores de classification sont obtenus pour les angles $\pm\texttt{5, 10, 15}$.
Mais dans le tableau \ref{Résultats_de_la_détection_des_visages_avec_différents_classifieurs}
on remarque qu'avec ces angles la précision baisse et le temps d’exécution augmente considérablement. 
J'ai donc choisi les angles: $\pm\texttt{5}^{\circ}$. 

\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=1\linewidth]{../images/rotate_symetric0.png}}
  \caption{Transformations sur les images positives}
  \label{Transformations_sur_les_images_positives}
\end{figure}

\subsubsubsection{Exemples négatifs}
Il faut ensuite prendre des exemples d'images négatifs qui ne contiennent pas de visages.
\\Cela est fait avec la méthode \texttt{\_generate\_negative\_image\_examples}. 

\vspace{5mm} 
Pour chaque image du dataset on prend \texttt{10} images négatives de taille \texttt{64x64} 
(\texttt{window\_shape}). 
Ces images ne doivent pas se superposer avec les vraies images du visage, 
et ne doivent pas se superposer entre elles.
Pour déterminer les images qui se superposent on utilise un \texttt{IOU} 
avec un seuil à \texttt{0.5} (\texttt{iou\_limit}).
Afin d'avoir des exemples diversifies, on redimensionne l'image d'origine en 4 
images de différentes échelles (\texttt{image\_layers}).
Les exemples négatifs sont alors pris aléatoirement à partir de ces images.

\subsubsection{Redimensionnement des images}
Afin de redimensionner l'image, on calcule le ratio par lequel on doit diviser la taille de l'image.
Sachant le nombre de redimensionnements souhaités, 
le ratio est calculé de manière à avoir la taille de la dernière image redimensionnée presque égale à la taille de la fenêtre glissante.
Cela permet de prendre en considération le cas où le visage prend toute la taille de l'image d'origine. 

\vspace{5mm} 
Au début j'ai utilisé la fonction \texttt{pyramid\_gaussian} de \texttt{skimage} pour faire le redimensionnement des images.
Mais cette méthode applique un filtre Gaussien sur les images avant de les redimensionner. 
Selon certains articles sur internet, ce filtre peut avoir un impact négatif sur la détection des visages.
Dans le tableau \ref{Résultats_de_la_détection_des_visages_avec_différents_paramètres} de la partie \ref{Détermination_des_meilleurs_paramètres},
on voit que cela est confirmé puisque les résultats de la détection sont moins bons avec le filtre.
J'ai donc réécrit la fonction \texttt{pyramid\_gaussian} en enlevant le filtre gaussien.
Cela est fait avec les méthodes \texttt{\_pyramid} et \texttt{\_get\_resized\_images}.
\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.8\linewidth]{../images/pyramid_leo.png}
  \caption{Redimensionnement des images}
  \label{}
\end{figure}

\subsubsection{Vecteurs descripteurs}
Afin de faire la classification des images, il faut entraîner le classifieur sur des 
vecteurs descripteurs.
Ces vecteurs peuvent être obtenus par deux méthodes: \texttt{HOG} et \texttt{sac de mots}.
\subsubsubsection{HOG}\label{HOG}
J'ai essayé de déterminer les meilleurs scores obtenus avec différents paramètres de \texttt{HOG}
sur un \texttt{SVC}.
On verra dans la partie \ref{Détermination_du_meilleur_classifieur} que le \texttt{SVC} est le meilleur classifieur.
J'ai utilisé une validation croisée \texttt{StratifiedShuffleSplit} à 3 splits.
J'ai testé les paramètres suivants:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item orientations
  \item pixels per cell
  \item cells per block
  \item block norm
\end{itemize}
On peut retrouver le code correspondant dans le fichier \path{params_optimization/best_classifier.py}, 
avec la fonction \texttt{best\_hog}.
On retrouve les résultats dans la figure \ref{Score_de_la_classification_avec_différents_paramètres_HOG} avec le temps d’exécution pour chaque test. 

\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=1.5\linewidth]{../images/hog_scores.png}}
  \caption{Score de la classification avec différents paramètres HOG}
  \label{Score_de_la_classification_avec_différents_paramètres_HOG}
\end{figure}

Le classifieur le plus rapide et avec les meilleurs scores est obtenu avec les paramètres suivants:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item orientations: 9
  \item pixels per cell: (8, 8)
  \item cells per block: (2, 2)
  \item block norm: L2
\end{itemize}

\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=0.5\linewidth]{../images/HOG.png}}
  \caption{Représentation du meilleur HOG sur une image de taille (64,64)}
  \label{Représentation_des_HOG_sur_une_image_de_taille_(64,64)}
\end{figure}

\subsubsubsection{Sac de mots}
Pour les sacs de mots j'ai testé deux options: \texttt{Sift} et \texttt{Daisy}.
J'ai utilisé une validation croisée \texttt{StratifiedShuffleSplit} à 3 splits avec un \texttt{SVC} avec différents paramètres.
J'ai testé également avec d'autres classifieurs.
On remarque dans les figures \ref{Score_Sift_avec_différents_nombres_de_clusters}
et \ref{Score_de_la_classification_avec_différents_paramètres_Daisy},
que la classification avec des sacs de mots a des scores plus 
faibles qu'avec les HOG. On utilisera donc les HOG.
\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=0.93\linewidth]{../images/sift_clusters_scores.png}}
  \caption{Score Sift avec différents nombres de clusters}
  \label{Score_Sift_avec_différents_nombres_de_clusters}
\end{figure}
\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=1\linewidth]{../images/daisy_clusters_scores_different_params.png}}
  \caption{Score de la classification avec différents paramètres Daisy}
  \label{Score_de_la_classification_avec_différents_paramètres_Daisy}
\end{figure}

\clearpage
\subsubsection{Apprentissage}
Avant de faire l'apprentissage, il faut choisir le classifieur qui a le meilleur score. 
Pour cela il faut tester différents algorithmes sur notre dataset avec une validation croisée. 
Il faut donc choisir aussi une validation croisée convenable.  
\subsubsubsection{Détermination de la meilleure validation croisée}
Dans mon dataset d'images il y a deux classes: \texttt{1:visage}, \texttt{-1:non visage}.
On a les -1 dans la première partie et les 1 dans la deuxième. 
On doit donc trier les images avant de faire les tests. 
C'est pour ces raisons que j'ai choisi un \texttt{StratifiedShuffleSplit} pour la validation croisée.
\subsubsubsection{Détermination du meilleur classifieur}\label{Détermination_du_meilleur_classifieur}
J'ai décidé de tester les classifieurs suivants avec une validation croisée à 3 splits:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item SVC
  \item Régression logistique
  \item KNN
  \item Classification naïve bayésienne
  \item SVC linéaire 
  \item Random Forest
  \item AdaBoost
\end{itemize}

On retrouve les scores dans la figure \ref{Score_de_différents_algorithmes_pour_le_premier_classifieur}.
On peut retrouver le code correspondant dans le fichier \path{params_optimization/best_classifier.py}, 
avec la fonction \texttt{best\_classifier}.

\vspace{5mm} 
On voit déjà que la plupart des algorithmes ont de très bons scores, avec le \texttt{SVC} qui a le meilleur.
Donc on pourrait penser que c'est le meilleur choix au début. 

\vspace{5mm} 
Néanmoins, il faut prendre en considération également le temps mis par chaque algorithme.
Dans la figure \ref{Score_de_différents_algorithmes_pour_le_premier_classifieur}, c'est le temps total de la validation croisée.
On remarque que le \texttt{SVC} est lent par rapport à la 
\texttt{Logistic Regression} et au \texttt{Linear SVC}, qui ont de bons scores, et qui sont les plus rapides.
Dans la partie \ref{Détermination_du_meilleur_classifieur} j'ai testé ces trois classifieurs sur la détection des visages. 
Le détecteur qui utilise le \texttt{SVC} est très lent par rapport aux deux autres mais c'est celui qui a les meilleurs résultats.
Comme le temps d’exécution n'est pas très important dans notre cas, j'ai choisi le \texttt{SVC}.
\begin{figure}[ht!]
  \makebox[\textwidth][c]{\includegraphics[width=1.5\linewidth]{../images/first_classifier_scores+++.png}}
  \caption{Score de différents algorithmes pour le premier classifieur}
  \label{Score_de_différents_algorithmes_pour_le_premier_classifieur}
\end{figure}

\clearpage
 
\subsection{Deuxième apprentissage}
\subsubsection{Ajout des faux positifs}
Avec la méthode \texttt{\_get\_false\_positives}, pour chaque image, on parcourt les différentes images redimensionnées avec une fenêtre glissante.
A chaque fois qu'il y a une prédiction positive, on vérifie si elle se superpose assez avec l'un des visages dans l'image.
Si l'iou est inférieure à 0.5 on l'ajoute à la liste des exemples négatifs, qui sera utilisée lors du deuxième apprentissage.  
\subsubsection{Apprentissage}
\subsubsubsection{Détermination du meilleur classifieur}
Pour le deuxième classifieur, on essaye aussi de déterminer le meilleur algorithme. 

Comme pour le premier classifieur, c'est le SVC qui donne le score (98.3 \%). 
On remarque aussi qu'en ajoutant les faux positifs, le score baisse un peu par rapport au premier classifieur, même s'il reste toujours très haut.
On peut essayer d'améliorer le SVC en utilisant un \texttt{GridSearchCV} avec les hyperparamètres suivants:   
\begin{itemize}
  \setlength\itemsep{0em}
  \item \textbf{kernel:} linear, rbf, poly, sigmoid
  \item \textbf{C:} 1, 10, 100
  \item \textbf{degree:} 2, 3, 4
  \item \textbf{gamma:} scale, auto
\end{itemize}

On prend en compte également le temps mis pour faire le \texttt{fit} et le \texttt{predict}.
On trouve le meilleur score à 98.7 \% avec les hyperparamètres suivants:
\begin{itemize}
  \setlength\itemsep{0em}
  \item \textbf{kernel:} poly
  \item \textbf{C:} 10
  \item \textbf{degree:} 4
  \item \textbf{gamma:} scale
\end{itemize}
On peut retrouver le code correspondant dans le fichier \path{params_optimization/best_classifier.py}, 
avec la fonction \texttt{tune\_hyperparameters}.
\clearpage

\section{Détecteur}
\subsection{Détection des visages}
Avec la méthode \texttt{fit}, on a créé un classifieur. 
On appelle maintenant la méthode \texttt{predict}, qui va utiliser ce classifieur pour faire les prédictions.  
Cela se fait avec la méthode \texttt{\_get\_predictions}, qui parcourt les différentes images redimensionnées avec une fenêtre glissante et récupère toutes les prédictions. 

\vspace{5mm}
On supprime ensuite les prédictions qui se superposent avec la méthode \texttt{\_delete\_overlapping\_windows}, 
et on enregistre les prédictions dans le fichier de sauvegarde s'il est indiqué.

\vspace{5mm} 
Si on donne les vrais labels à la méthode \texttt{predict}, 
les performances du détecteur peuvent êtres calculées avec les méthodes \texttt{\_get\_true\_predictions} et \texttt{\_get\_metrics}.
On pourra ainsi connaître, la précision, le rappel, le score F1 et l'aire sous la courbe Précision/Rappel. 

\subsection{Amélioration du détecteur}
\subsubsection{Détermination des meilleurs hyperparamètres}\label{Détermination_des_meilleurs_paramètres}
Afin d'améliorer les performances du détecteur de visage, on peut commencer par voir l'impact de différents hyperparamètres sur les métriques.  
Pour cela j'ai effectué plusieurs tests avec un \texttt{StratifiedShuffleSplit} avec 3 splits, 
et un \texttt{SVC} avec les hyperparamètres suivants: \texttt{C=10, kernel='poly', degree=4, gamma='scale'}. 
Pour ces tests j'ai utilisé le HOG avec ses paramètres par défaut. Néanmoins dans le code final
ce sont les paramètres qui sont obtenus dans la partie \ref{HOG} qui sont utilisés.

\vspace{5mm} 
La validation croisée permet de faire la prédiction sur des images qui n'ont pas été 
utilisées pour entraîner le classifieur. 
Le code correspondant se trouve dans le fichier \path{params_optimization/best_params.py}. 
J'ai fait du \texttt{multiprocessing}, afin d'obtenir des résultats plus rapides.

\vspace{5mm} 
On obtient les résultats dans le tableau \ref{Résultats_de_la_détection_des_visages_avec_différents_paramètres}. 
\clearpage

\begin{table}[ht!]
  \centering
  \begin{adjustbox}{width=1.4\textwidth, center=\textwidth}
      \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}
      \hline
      Taille de la fenêtre & 32x32 & \textbf{64x64} & 32x32 & 32x32 &  32x32 & 32x32 & 32x32 & 32x32 & 32x32 & 32x32\\ \hline
      Nbr. redimensionnements & 4 & 4 & \textbf{8} & 4 & 4 & 4 & 4 & 4 & 4 & 4 \\ \hline
      Pas & 15 & 15 & 15 & \textbf{10} & 15 & 15 & 15 & 15 & 15 & 15\\ \hline
      Filtre gaussien & & & & & \textbf{x} & & & & &\\ \hline
      Scaler & & & & & & \textbf{MinMax} & \textbf{Standard} & & & \\ \hline 
      Rotation & & & & & & & & \textbf{$\pm$ 5,10,15} & & \\ \hline
      Symétrie & & & & & & & & & \textbf{x} &\\ \hline
      Nbr. exemples neg. / im & 10 & 10 & 10 & 10 & 10 & 10 & 10 & 10 & 10 & \textbf{20}\\ \Xhline{2\arrayrulewidth}
      Temps hh:mm & 00:12 & 01:18 & 00:18 & 00:27 & 00:12 & 00:12 & 00:14 & 00:19 & 00:14 & 00:12\\ \hline
      Précision & 61.12 & 89.59 & 58.69 & 70.13 & 58.93 & 55.69 & 52.38 & 44.73 & 55.45 & 56.41\\ \hline
      Rappel & 15.18 & 46.96 & 16.82 & 23.59 & 14.64 & 13.70 & 14.56 & 19.85 & 18.22 & 14.71\\ \hline
      \textbf{F1} & \textbf{24.32} & \textbf{61.62} & \textbf{26.15} & \textbf{35.31} & \textbf{23.45} & \textbf{22.00} & \textbf{22.79} & \textbf{27.50} & \textbf{27.43} & \textbf{23.34} \\ \hline
      \textbf{AUC} & \textbf{11.84} & \textbf{45.32} & \textbf{13.31} & \textbf{19.87} & \textbf{11.45} &\textbf{10.29} & \textbf{10.18} & \textbf{14.01} & \textbf{13.13} & \textbf{11.07} \\ \hline
      \end{tabular} 
  \end{adjustbox}  
    \caption{Résultats de la détection des visages avec différents hyperparamètres}
    \label{Résultats_de_la_détection_des_visages_avec_différents_paramètres}
\end{table}

On remarque que les résultats sont largement améliorés avec \textbf{une fenêtre plus grande}, 
et dans une moindre mesure avec \textbf{un pas plus faible}.
\textbf{Un nombre de redimensionnements plus élevé},\textbf{une symétrie} et \textbf{une rotation}
des images permettent aussi d'avoir de meilleurs résultats.
En effet, améliorer le pas et le nombre de redimensionnements permet de voir plus d'images et donc faire plus de prédictions.
Par ailleurs, avec des rotations et des symétries on peut entraîner le classifieur sur plus d’exemples.

\vspace{5mm} 
Par contre le filtre gaussien, la normalisation et la standardisation des images et un
nombre d'images négatives plus élevé semblent diminuer les performances.
\subsubsection{Détermination du meilleur classifieur}\label{Détermination_du_meilleur_classifieur}
On essaye maintenant de combiner les meilleurs hyperparamètres précédents pour avoir les meilleurs résultats.
Les meilleurs résultats qu'on a obtenu avec les tests précédents sont ceux qui ont mis le plus de temps. 
En combinant les meilleurs hyperparamètres, le temps d’exécution devient très important avec le \texttt{SVC} précédent,
comme on peut le voir dans le tableau \ref{Résultats_de_la_détection_des_visages_avec_différents_classifieurs}. 

\vspace{5mm} 
J'ai donc essayé de diminuer ce temps en utilisant d'autres classifieurs, plus rapides, 
mais qui ont quand même de bons scores pour la prédiction des visages.
\clearpage

\begin{table}[ht!]
  \centering
  \begin{adjustbox}{width=1.4\textwidth, center=\textwidth}
      \begin{tabular}{|c|c|c|c|c|c?c|c|c|c|c|}
      \hline
      Classifieur & Taille & Nbr. redim & Pas & Rotation & Symétrie & Temps & Précision & Rappel & \textbf{F1} & \textbf{AUC} \\ \hline
      SVC & 64x64 & 10 & 8 & & & 05:52 & 99.13 & 80.68 & \textbf{88.96} & \textbf{80.26} \\ \hline
      SVC & 64x64 & Tr: 4 \vline Ts: 10 & Tr: 15 \vline Ts: 8 & & & 02:40 & 93.90 & 87.53 & \textbf{90.60} & \textbf{85.75} \\ \hline
      SVC & 64x64 & Tr: 4 \vline Ts: 10 & Tr: 15 \vline Ts: 8 & & x & 03:17 & 94.39 & 90.42 & \textbf{92.36} & \textbf{88.20} \\ \hline
      SVC & 64x64 & Tr: 4 \vline Ts: 10 & Tr: 15 \vline Ts: 8 & $\pm$ 5,10,15 & x & 10:43 & 80.39 & 92.91 & \textbf{86.19} & \textbf{87.21} \\ \hline
      SVC & 64x64 & Tr: 4 \vline Ts: 10 & Tr: 15 \vline Ts: 8 & $\pm$ 5 & x & 04:13 & 92.86 & 91.19 & \textbf{90.23} & \textbf{88.61} \\ \hline
      SVC & 64x64 & 10 & Tr: 8 \vline Ts: 6 & $\pm$ 5 & x & 13:28 & 98.91 & 91.90 & \textbf{95.27} & \textbf{91.33} \\ \hline

      Linear SVC & 64x64 & 10 & 8 & & & 01:50 & 79.12 & 80.29 & \textbf{79.70} & \textbf{73.23} \\ \hline
      Logistic regression & 64x64 & 10 & 8 & & & 01:45 & 92.85 & 75.93 & \textbf{83.54} & \textbf{74.22} \\ \hline
      Logistic regression & 64x64 & 12 & 6 & $\pm$ 5 & x & 03:09 & 88.14 & 86.43 & \textbf{87.48} & \textbf{81.60} \\ \hline
    \end{tabular} 
  \end{adjustbox}  
  {\raggedright\small\textit{Tr: Train, Ts: Test}  \par}
  \caption{Résultats de la détection des visages avec différents classifieurs}
  \label{Résultats_de_la_détection_des_visages_avec_différents_classifieurs}
\end{table}

On remarque que les classifieurs \texttt{Logistic Regression} et \texttt{Linear SVC} sont beaucoup plus rapides, 
mais ils ont des métriques plus faibles. 
On peut se permettre néanmoins d'optimiser encore les hyperparamètres du pas et du nombre de redimensionnements.
Mais on n'arrive toujours pas aux performances du \texttt{SVC}. 
En effet, si on diminue le pas, on améliore le Rappel, mais la Précision baisse puisqu'on fait beaucoup plus de prédictions.  

On remarque également qu'augmenter le nombre de rotations des images positives, améliore
légèrement le rappel mais fait baisser la précision et augmente le temps d’exécution. 
J'ai donc décidé de me contenter d'une rotation avec un angle $\pm$ 5.

\subsection{Prédictions sur les données de test}
J'ai la possibilité d'obtenir les métriques du détecteur sur les vrais données de tests.
J'ai le droit à trois essais, j'en ai fait deux. A chaque fois j'ai choisi les meilleurs hyperparamètres obtenus
dans le tableau \ref{Résultats_de_la_détection_des_visages_avec_différents_classifieurs}.

Le tableau \ref{Résultats_de_la_détection_des_visages_sur_les_images_de_test} 
résume les résultats obtenus.
\begin{table}[ht!]
  \centering
  \begin{adjustbox}{width=1\textwidth, center=\textwidth}
      \begin{tabular}{|c|c|c|}
      \hline
      Classifieur & SVC & SVC \\ \hline
      Taille fenêtre & 64x64 & 64x64  \\ \hline
      Nbr redimensionnements & Tr: 4 Ts: 10 & 10 \\ \hline
      Pas & Tr: 15   Ts: 8 & Tr: 8   Ts: 6 \\ \hline
      Rotation & & $\pm$ 5 \\ \hline
      Symétrie & x & x  \\ \Xhline{2\arrayrulewidth}
      Temps hh:mm & Tr: 01:34 Ts: 04:00 & Tr: 07:15  Ts: 15:25 \\ \hline
      Précision & 94.78 & 99.07  \\ \hline
      Rappel & 92.62 &  94.02 \\ \hline
      \textbf{F1} & \textbf{93.69} & \textbf{96.48} \\ \hline
      \textbf{AUC} & \textbf{91.21} & \textbf{93.46} \\ \hline
      \end{tabular} 
  \end{adjustbox}  
    \caption{Résultats de la détection des visages sur les images de test}
    \label{Résultats_de_la_détection_des_visages_sur_les_images_de_test}
\end{table}

    

\section{Conclusion}
Le deuxième détecteur obtenu a de très bonnes métriques sur les données de test.
Mais le temps d’exécution est très long (plus de 22 heures), 
avec 90s en moyenne pour prédire les visages sur une image. 
Dans notre cas, le temps d’exécution n'est pas très important. C'est donc lui qui sera choisi. 
Mais ce détecteur ne peut pas être utilisé
pour une application en temps réel. 
Il faudrait utiliser des réseaux de neurones à la place par exemple. 

\end{document}
import os
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from itertools import combinations
import random
import math
from math import exp, log
from joblib import dump, load
from tqdm import tqdm

from skimage import io, util
from skimage.color import rgb2gray
from skimage.transform import pyramid_gaussian, resize, rotate
from skimage.feature import hog
from skimage.util import view_as_windows
from skimage.feature import daisy

from sklearn.svm import SVC
from sklearn.metrics import auc
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import KMeans

from utils import get_execution_time, show_windows_on_image

from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC

class face_detector():
    ''' Class used to perform face detection'''

    def __init__(self, window_shape=(64, 64), image_layers=10, step=6, iou_limit=0.5, scaler=None, feature_extractor='HOG'):
        '''
        Parameters
        ----------
        window_shape (tuple): shape of the sliding window, default=(64,64)
        image_layers (int): number of image layers, default=10
        step (int): step of the sliding window, default:6
        iou_limit (int): iou_limit for the sliding window, default:0.5
        scaler: scaler to use, only StandardScaler and MinMaxScaler are accepted, default=None
        feature_extractor (str): feature extractor to use. 'HOG' for Histogram of Oriented Gradients, 'BOW' for Daisy, default:'HOG'
        '''

        self._window_shape = window_shape
        self._image_layers = image_layers
        self._step = step
        self._iou_limit = iou_limit

        self._scaler = scaler
        self._use_scaler = False if scaler == None else True
        self._feature_extractor = feature_extractor

        print('Hyperparameters:')
        print(f'window shape: {self._window_shape}')
        print(f'step: {self._step}')
        print(f'layers: {self._image_layers}')
        print(f'iou limit: {self._iou_limit}\n')

    def fit(self, X, y, train_path, symetric=True, rotation=True, clf_path='classifier.joblib'):
        '''
        Train the face classifier

        Parameters
        ----------
        X: array-like
            Training images
        y: array-like
            labels
        train_path: str
            path to train images folder
        symetric: bool, default=True
            if True, train the classifier on symetric faces
        rotation: bool, default=True
            if True, train the classifier on rotated faces
        clf_path: str, default: 'classifier.joblib'
            path where to save the classifier

        Returns
        ----------
        clf: classifier
        '''
        start = time.time()
        self._change_labels_to_square(y, train_path)

        # Generate positive and negative image examples
        X_clf_positive = self._get_positive_images(X, y, symetric=symetric, rotation=rotation)
        X_clf_negative = self._generate_negative_image_examples(X, y)

        Y_clf = self._get_image_labels(X_clf_positive, X_clf_negative)
        X_clf = np.concatenate((X_clf_positive, X_clf_negative))
        
        del X_clf_positive, X_clf_negative
        X_clf = self._preprocess(X_clf)
        
        # Train first classifier
        print('>>Training first classifier')
        clf = SVC(C=10, kernel='poly', degree=4, gamma='scale')
        clf.fit(X_clf, Y_clf)
        
        # Generate false positives
        false_positives = self._get_false_positives(X, y, clf)

        if false_positives.shape[0] != 0:
            X_clf = np.concatenate((X_clf, false_positives))
            X_clf = self._preprocess(X_clf)
            Y_clf = np.append(Y_clf, [-1] * (X_clf.shape[0] - Y_clf.shape[0]))

        # Train second classifier
        print('>>Traininig second classifier')
        clf = SVC(C=10, kernel='poly', degree=4, gamma='scale')
        clf.fit(X_clf, Y_clf)
        
        del X_clf, Y_clf
        
        print(f'>>Saving classifier in {clf_path}\n')
        dump(clf, clf_path) 
        get_execution_time(start, 'Fit duration')
        
        return clf

    def predict(self, X, y=None, predictions_path=None, append_predictions=False, clf_path='classifier.joblib', visualize=False):
        '''
        Predict face position

        Parameters
        ----------
        X: array-like
            Testing images
        y: array-like, default None
            labels. if given, compute metrics to evaluate the predictions
        predictions_path: str, default None
            file path where to store the predictions. predictions are not stored if not given
        append_predictions: Bool, default False
            if True, append predictions to the predictions file
        clf_path: str, default: 'classifier.joblib'
            path where to load the classifier from
        visualize: bool, default:False
            if True, show predictions on image during predict
        
        Returns
        ----------
        predictions: pandas DataFrame
        '''
        start = time.time()

        #Load classifier trained in fit method
        print(f'>>Loading classifier from {clf_path}\n')
        clf = load(clf_path)
        
        #remove previous predictions
        if predictions_path != None and os.path.exists(predictions_path) and not append_predictions:
            os.remove(predictions_path)
        
        if isinstance(y, pd.DataFrame):
            predictions = self._get_predictions(X=X, clf=clf, save_path=predictions_path, images_index=set(y.image_index), visualize=visualize)
            predictions, nbr_faces_detected = self._get_true_predictions(predictions, y)
            F1, AUC, Precision, Recall = self._get_metrics(predictions, y, nbr_faces_detected)
        
        else:
            predictions = self._get_predictions(X=X, clf=clf, save_path=predictions_path, visualize=visualize)

        get_execution_time(start, 'Prediction duration')

        return predictions

    
    def _change_labels_to_square(self, y, train_path, outside=True):
        '''
        Change labels to square.
        
        Parameters
        ----------
        y: array-like
            labels
        train_path: str
            path to train images folder
        outside: Bool, default: True
            if True, the label square is outside the original rectangle, otherwise it's inside
        
        Returns
        ----------
        square_labels: pandas DataFrame 
        '''
        print('>>Changing labels to squares')
        labels = y.copy()
        #Add the original image height and width
        images_shapes = [io.imread(os.path.join(train_path, '%04d.jpg'%image_index)).shape[0:2] for image_index in labels.image_index]
        labels['image_height'] = [shape[0] for shape in images_shapes]
        labels['image_width'] = [shape[1] for shape in images_shapes]

        height_width_diff = np.abs((labels['height'] - labels['width']))
        
        if outside:
            # The square label is outside the original rectangle

            #Add the max between the height and the width
            labels['max_width_height'] = labels[['height','width']].max(axis=1)

            #Change the label position to be centered on the rectangle
            labels.loc[labels['height'] > labels['width'], 'j'] = labels['j'] - (height_width_diff // 2)
            labels.loc[labels['height'] < labels['width'], 'i'] = labels['i'] - (height_width_diff // 2)

            #Shift the new label if it's outside the original image borders
            labels.loc[labels['j'] + labels['max_width_height'] > labels['image_width'], 'j'] = labels['image_width'] - labels['max_width_height']
            labels.loc[labels['i'] + labels['max_width_height'] > labels['image_height'], 'i'] = labels['image_height'] - labels['max_width_height']
            labels.loc[labels['j'] <0, 'j'] = 0
            labels.loc[labels['i'] <0, 'i'] = 0
        else:
            # The square label is inside the original rectangle

            #Add the min between the height and the width
            labels['min_width_height'] = labels[['height','width']].min(axis=1)
            #Change the label position to be centered on the rectangle
            labels.loc[labels['height'] > labels['width'], 'i'] = labels['i'] + (height_width_diff)
            labels.loc[labels['height'] < labels['width'], 'j'] = labels['j'] + (height_width_diff)

        self.square_labels = labels
        

    def _get_positive_images(self, X, y, symetric=True, rotation=True):
        '''
        Get face images. Images shape is equal to window_shape
        
        Parameters
        ----------
        X: array-like 
            Training images
        y: array-like
            labels
        symetric: bool, default=True
            if True, train the classifier on symetric faces
        rotation: bool, default=True
            if True, train the classifier on rotated faces
        
        Returns
        ----------
        pos_examples: array-like
            array of the face images 
        '''
        print('>>Generating positive images')
        def _rotate_resize(image, label):
            for angle in [-5, 5]:
                new_image = _resize(rotate(image, angle, center=(label[1]+ int(label[3]/2),label[0]+ int(label[2]/2))), label)
                yield new_image
        def _resize(image, label):
            return resize(image[label[0]: label[0] + label[2], label[1]: label[1] + label[3]], self._window_shape)
        
        labels = self.square_labels
        pos_examples = []
        #Iterate over the train images
        for it, image_index in enumerate(tqdm(set(y.image_index))):
            image = self._scaler.fit_transform(rgb2gray(X[it])) if self._use_scaler else rgb2gray(X[it])
            positive_labels = labels[labels.image_index == image_index][['i','j','max_width_height','max_width_height']].to_numpy()
            #Iterate over the image face labels
            for label in positive_labels:
                #Resize the image to the required size and append it to the examples
                pos_examples.append(self._extract_features(_resize(image, label)))

                #Rotate the image in the following angles: [-5, 5]
                if rotation:
                    [pos_examples.append(self._extract_features(img)) for img in _rotate_resize(image, label)]
                #Add the symetric face
                elif symetric:
                    pos_examples.append(self._extract_features(np.fliplr(_resize(image, label))))
                elif rotation and symetric:
                    pos_examples.append(self._extract_features(np.fliplr(_resize(image, label))))
                    [pos_examples.append(self._extract_features(img)) for img in _rotate_resize(image, label)]
                    # [pos_examples.append(self._extract_features(np.fliplr(img))) for img in _rotate_resize(image, label)]
                
        pos_examples = np.array(pos_examples)

        print(f'Number of positive images: {pos_examples.shape[0]}\n')
        return pos_examples

    def _generate_negative_image_examples(self, X, y):
        '''
        Genrate images different from the face images. Images shape is equal to window_shape
        
        Parameters
        ----------
        X: array-like 
            Training images
        y: array-like
            labels
        
        Returns
        ----------
        neg_examples: array-like
            array of the images 
        '''
        print('>>Generating negative images')
        neg_examples = []

        labels = self.square_labels
        random.seed(42)
        #Iterate over the train images
        for it, image_index in enumerate(tqdm(set(y.image_index))):
            image = self._scaler.fit_transform(rgb2gray(X[it])) if self._use_scaler else rgb2gray(X[it]) 
            resized_images, image_downscale = self._get_resized_images(image)
            positive_labels = labels[labels.image_index == image_index][['i', 'j', 'max_width_height', 'max_width_height']].to_numpy()
            nbr_neg_examples = 10
            
            neg_examples_params = []
            # Generate images while the number of examples is lower than nbr_neg_examples
            while len(neg_examples_params) < nbr_neg_examples:
                overlap = False
                example, params = self._generate_negative_example(resized_images, image_downscale, positive_examples = positive_labels)
                #Iterate over the already generated negative images for the current image
                for ex in neg_examples_params:
                    original_size_window = [int(item * params[2]) for item in [params[0], params[1], self._window_shape[0], self._window_shape[1]]]
                    #Check if the current negative image overlaps over the previous negative images
                    if self._IoU(original_size_window, (ex[0],ex[1],self._window_shape[0], self._window_shape[1])) > self._iou_limit:
                        overlap = True
                        break
                #Add the current negative image if there is no overlap
                if not overlap:
                    features = self._extract_features(example)
                    if not isinstance(features, type(None)):
                        neg_examples.append(self._extract_features(example))
                        del example
                        neg_examples_params.append([params[0]*params[2], params[1]*params[2]])

        neg_examples = np.array(neg_examples)
        print(f'Number of negative images: {neg_examples.shape[0]}\n')

        return neg_examples

    def _get_image_labels(self, positive_images, negative_images):
        '''
        Create face image labels for the classifier

        Parameters
        ----------
        positive_images: array-like
        negative_images: array-like
        
        Returns
        ----------
        Y_clf: array-like
        '''
        pos_size = positive_images.shape[0] 
        neg_size = negative_images.shape[0] 
        Y_clf = np.array([1 if i < pos_size  else -1 for i in range(0,(pos_size + neg_size))])
        return Y_clf

    def _get_false_positives(self, X, y, clf):
        '''
        Sliding window that returns False positive face predictions

        Parameters
        ----------
        X: array-like
            Training images
        y: array-like
            labels
        clf: classifier
            classifier trained the first time

        Returns
        ----------
        false_positives: array-like
        '''
        print('>>Generating False Positives')
        false_positives = []
        #Iterate over train images
        for it, image_index in enumerate(tqdm(set(y.image_index))):
            # Stop getting images to avoid using a lot of memory
            if len(false_positives) >= 20000:
                break
            gray_image = self._scaler.fit_transform(rgb2gray(X[it])) if self._use_scaler else rgb2gray(X[it]) 
            #Positive labels for the current image
            positive_labels = y[y.image_index == image_index][['i', 'j', 'height', 'width']].to_numpy()
        
            resized_images, image_downscale = self._get_resized_images(gray_image)

            #iterate over the image layers
            for layer_nbr, image in enumerate(resized_images):
                # the current layer ratio
                ratio = np.power(image_downscale, layer_nbr) 
                #all possible windows in the current layer
                windows = view_as_windows(image, self._window_shape, self._step)
                #iterate over rows
                for i, row in enumerate(windows):
                    i = i * self._step
                    #iterate over columns
                    for j, window in enumerate(row):
                        j = j * self._step
                        #get window position and size in the original image layer, in order to compute its iou with the true labels
                        original_window_params = [int(item * ratio) for item in [i, j, self._window_shape[0], self._window_shape[1]]]
                        false_positive = True
                        #extract features
                        features = self._extract_features(window)
                        if not isinstance(features, type(None)):
                            features_exist = True
                            features = self._preprocess(features, train=False)
                        else:
                            features_exist = False
                        #check if true prediction
                        if features_exist and clf.predict(features) == 1:
                            #compute iou with every true label
                            for label_params in positive_labels:
                                if self._IoU(original_window_params, label_params) > self._iou_limit:
                                    false_positive = False
                                    break
                            if false_positive:
                                false_positives.append(self._extract_features(window))

        false_positives = np.array(false_positives)
        print(f'Number of false positives: {len(false_positives)}\n')
        return false_positives


    def _get_predictions(self, X, clf, save_path=None, images_index=None, visualize=False):
        '''
        Sliding window that returns face predictions

        Parameters
        ----------
        X: array-like
            Training images
        clf: classifier
            classifier trained the second time
        save_path: str
            paths where to save the predictions
        image_index: set
            index of the images to make the predictions on. Given if not all the train images are used, in cross-validation for example.
        visualize: bool, default:False
            if True, show predictions on image during predict
        Returns
        ----------
        predictions: pd DataFrame
        '''
        print('>>Detecting faces')
        predictions = []

        images_index = range(1, X.shape[0]+1) if images_index == None else images_index
        #iterate over the train images
        for it, image_index in enumerate(tqdm(images_index)):
            current_predictions = []
            gray_image = self._scaler.fit_transform(rgb2gray(X[it])) if self._use_scaler else rgb2gray(X[it])
            resized_images, image_downscale = self._get_resized_images(gray_image)

            #iterate over the layers
            for layer_nbr, image in enumerate(resized_images):
                ratio = np.power(image_downscale, layer_nbr) 
                windows = view_as_windows(image, self._window_shape, self._step)

                for i, row in enumerate(windows):
                    i = i * self._step
                    for j, window in enumerate(row):
                        j = j * self._step
                        original_window_params = [int(item * ratio) for item in [i, j, self._window_shape[0], self._window_shape[1]]]
                        features = self._extract_features(window)
                        
                        if not isinstance(features, type(None)):
                            features_exist = True
                            features = self._preprocess(features, train=False)
                            decision_functions = clf.decision_function(features)
                        else:
                            features_exist = False

                        #add the current widow params if a face is predicted
                        if features_exist and decision_functions[0] > 0:
                            params = [image_index]
                            [params.append(param) for param in original_window_params]
                            score = round(decision_functions[0], 2)
                            params.append(score)
                            current_predictions.append(params)
                            predictions.append(params)

            if visualize:
                show_windows_on_image(X[it], current_predictions)

        predictions = pd.DataFrame(predictions, columns=['image_index', 'i', 'j', 'height', 'width', 'score'])
        #delete overlapping windows
        predictions = self._delete_overlapping_windows(predictions)
        #save the predictions if a save_path is given
        if save_path != None:
            with open(save_path, 'a+') as f:
                for prediction in predictions.to_numpy():
                    [f.write('{} '.format(int(param))) for param in prediction[0:5]]
                    f.write('{}\n'.format(prediction[5]))
        return predictions
    
    def _delete_overlapping_windows(self, predictions):
        '''
        Delete overlapping windows from predictions

        Parameters
        ----------
        predictions: pd DataFrame

        Returns
        ----------
        predictions: pd DataFrame
        '''
        print('>>Deleting overlapping windows')
        predictions['idx'] = predictions.index 
        windows_to_delete = []
        #iterate over each image
        for image_index in set(predictions.image_index):
            windows = predictions[predictions.image_index == image_index]
            windows = windows.reset_index(drop=True)
            #iterate over every prediction combination
            for pair in combinations(range(len(windows)), r=2):
                window1 = windows.loc[pair[0]]
                window2 = windows.loc[pair[1]]
                #if iou > self._iou_limit add window with the lower score to the windows to be deleted
                if self._IoU(window1[['i', 'j', 'height', 'width']], window2[['i', 'j', 'height', 'width']]) > self._iou_limit:
                    windows_to_delete.append(window2.idx) if window1[5] > window2[5] else windows_to_delete.append(window2.idx)
        nbr_windows_before = predictions.shape[0]
        #delete windows
        predictions = predictions[~predictions.idx.isin(windows_to_delete)]
        nbr_windows_after = predictions.shape[0]
        print('Number of deleted windows: {}\n'.format(nbr_windows_before- nbr_windows_after))
        return predictions[['image_index', 'i', 'j', 'height', 'width', 'score']]


    def _get_true_predictions(self, predictions, true_labels):
        '''
        Compare predicted windows to real labels

        Parameters
        ----------
        predictions: pd DataFrame
        true_labels: pd DataFrame
        
        Returns
        ----------
        predictions: pd DataFrame
            predictions with an additional column saying whether each prediction is a true or false positive
        '''
        print('>>Comparing predictions to labels')
        predictions['idx'] = predictions.index
        #all the windows are assumed to be false predictions
        predictions['TP'] = False
        TP_index = []
        nbr_faces_detected = 0
        #iterate over each image
        for image_index in set(true_labels.image_index):
            images = true_labels[true_labels.image_index == image_index]
            windows = predictions[predictions.image_index == image_index].sort_values(by=['score'], ascending=False)
            #iterate over each window in the image
            for _, window in windows.iterrows():
                #iterate over each true label in the image
                for id_image, image in images.iterrows():
                    if self._IoU(image[['i', 'j', 'height', 'width']], window[['i', 'j', 'height', 'width']]) > self._iou_limit:
                        #add the window as a true prediction
                        TP_index.append(window.idx)
                        nbr_faces_detected += 1
                        #remove the predicted label from the current image labels
                        images = images.drop(index=id_image)
                        break

        #modify the TP column for the truly predicted windows
        predictions['TP'] = predictions.idx.isin(TP_index)
        return predictions, nbr_faces_detected


    def _get_metrics(self, predictions, true_labels, nbr_faces_detected, plot_curve=False):
        '''
        Compute the precision, recall, F1-score and AUC

        Parameters
        ----------
        predictions: pd DataFrame
        true_labels: pd DataFrame
        nbr_faces_detected: int
        plot_curve: Bool, default: False
            if True plot the precision/recall curve

        Returns
        ----------
        F1: float
        AUC: float
        '''
        print('>>Getting metrics')
        #Get True positives, False positives and False negatives
        tp_count = (predictions['TP'] == True).value_counts()
        if tp_count.shape[0] == 2:
            #we have True and False predictions
            TP, FP = tp_count[True], tp_count[False] 
        elif tp_count.keys()[0] == False:
            #we only have False predictions
            TP, FP = 0, predictions.shape[0] 
        else:
            #we only have True predictions
            TP, FP = predictions.shape[0], 0  
        FN = true_labels.shape[0] - nbr_faces_detected

        #Calculate Precision and Recall
        Precision = TP / (TP + FP)
        Recall = TP / (TP + FN)
        
        #Calculate F1-score
        F1 = (2 * (Precision * Recall) / (Precision + Recall)) if TP != 0 else None

        # Draw Recall/Precision curve
        number_of_faces = true_labels.shape[0]
        windows = predictions.sort_values(by=['score'], ascending=False)
        tp = 0
        fp = 0
        fn = number_of_faces
        precision = []
        recall = []
        for _, window in windows.iterrows():
            if window.TP:
                tp += 1
                fn -= 1
            else:
                fp += 1

            precision.append(tp / (tp + fp))
            recall.append(tp / (tp + fn))
        
        #Calculate Area under curve
        AUC = auc(recall, precision) if TP != 0 else None


        print('Nbr. Predictions: ', predictions.shape[0])
        print('Nbr. True Positive: ', TP)
        print('Nbr. False Positive: ', FP, '\n' )

        print('Nbr. Faces: ', true_labels.shape[0])
        print('Nbr. Faces detected: ', nbr_faces_detected)
        print('Nbr. Faces not detected (FN): ', FN, '\n')

        print('Precision: ', Precision)
        print('Recall: ', Recall)
        print('F1: ', F1)
        print('AUC: ', AUC)

        if plot_curve:
            fig = plt.figure(figsize=(8, 8))
            plt.plot(recall, precision)
            plt.axis((0, 1, 0, 1))
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.show()
        return F1, AUC, Precision, Recall

    def _IoU(self, window1, window2):
        '''
        Calculate iou between two windows

        Parameters
        ----------
        window1: list
        window2: list
            both windows contain the following args:
            i: int 
                row index of the window
            j: int 
                column index of the window
            h: int
                height of the window
            w: int
                width of the window
            
        Returns
        ----------
        iou: float
        '''
        i1, j1, h1, w1 = window1
        i2, j2, h2, w2 = window2

        top = max(i1, i2)
        bottom = min(i1 + h1, i2 + h2)
        left = max(j1, j2)
        right = min(j1 + w1, j2 + w2) 
        
        overlap_height = bottom - top
        overlap_width = right - left 
        
        if overlap_height <=0 or overlap_width <=0:
            overlap_area = 0
        else:
            overlap_area = overlap_height * overlap_width

        area1 = h1 * w1
        area2 = h2 * w2
        
        union_area = area1 + area2 - overlap_area
        iou = overlap_area / union_area
        
        return iou

    def _pyramid(self, image, downscale):
        '''
        Resize the image to different layers. Same as skimage.transform.pyramid_gaussian but without the Gaussian filter

        Parameters
        ----------
        image: array-like
            image to resize
        downscale: float
            ratio to resize the window by    
        
        Returns
        ----------
        image generator
            Generator yielding image layers 
        '''
        #yield the original image
        yield image
        # build downsampled images until max_layer is reached
        layer = 0
        while layer != self._image_layers:
            layer += 1
            out_shape = tuple([math.ceil(d / float(downscale)) for d in image.shape])
            image = resize(image, out_shape, anti_aliasing=False)
            yield image

    def _get_resized_images(self, image):
        '''
        Return different image layers.
        The downscale is calculated in order to have a number of additional layers corresponding to self._image_layers. 
        The same downscale is used each time.
        The last layer shape is as close as possible to self._window_shape 
        
        Parameters
        ----------
        image: array-like
            image to resize
        
        Returns
        ----------
        resized_images: list
            list of the resized layers
        downscale: float
            downscale used to resize the images
        '''
        downscale = exp(log(min(image.shape)/self._window_shape[0])/self._image_layers)
        resized_images = tuple(self._pyramid(image, downscale)) 
        
        # resized_images = tuple(pyramid_gaussian(image, downscale=downscale, multichannel=False, max_layer= self._image_layers))
        return resized_images, downscale


    def _bow(self, X, train):
        '''
        Genrate the histogram of the Bag of Words
        
        Parameters
        ----------
        X: array-like 
            features
        train: bool
            if True, the function is called during training, else during testing

        Returns
        ----------
        histogram: array-like

        '''
        def words2hist(words, n_clust):
            hist = np.bincount(words, minlength=n_clust)
            hist = hist/np.sum(hist)
            return hist.reshape((1,len(hist)))

        
        if train:
            features = np.concatenate(X, axis=0)
            self.n_clusters = 20
            self.kmeans = KMeans(n_clusters=self.n_clusters)
            print('>>Running kmeans')
            words = self.kmeans.fit_predict(features)
            histogram = np.zeros((X.shape[0], self.n_clusters))
            words_nbr = 0

            for i in tqdm(range(X.shape[0])):
                feature_nbr = X[i].shape[0]
                histogram[i, :] = words2hist(words[words_nbr:words_nbr + feature_nbr], self.n_clusters)
                words_nbr += feature_nbr

            histogram = StandardScaler().fit_transform(histogram)
            return histogram
        
        else:
            word = self.kmeans.predict(X)
            return words2hist(word, self.n_clusters) 
    
    def _preprocess(self, X, train=True):
        '''
        Returns histogram of bag of words or HOG features
        
        Parameters
        ----------
        X: array-like 
            features
        train: bool
            if True, the function is called during training, else during testing

        '''
        if self._feature_extractor == 'BOW':
            return self._bow(X, train)

        elif self._feature_extractor == 'HOG':
            return X if train else [X]
        else:
            raise ValueError(f'Unkown feature extractor {self._feature_extractor}. Accepting only "HOG" or "BOW".')

    def _extract_features(self, image):
        '''
        Extract HOG or Daisy features

        Parameters
        ----------
        image: array-like
        
        Returns
        ----------
        features: array-like
        '''
        if self._feature_extractor == 'HOG':
            self.orientations = 9
            self.pixels_per_cell = (8, 8)
            self.cells_per_block = (2, 2)
            self.block_norm = 'L2'
            
            return hog(image, orientations=self.orientations, pixels_per_cell=self.pixels_per_cell, cells_per_block=self.cells_per_block, block_norm=self.block_norm)

        elif self._feature_extractor == 'BOW':
            descriptors = daisy(image, step=16, radius=15, rings=3, histograms=8, orientations=8, normalization='l1', visualize=False) 
            descriptors = descriptors.reshape((descriptors.shape[0]*descriptors.shape[1], descriptors.shape[2]))
            return descriptors

        else:
            raise ValueError(f'Unkown feature extractor {self._feature_extractor}. Accepting only "HOG" or "BOW".')
    
    def _generate_negative_example(self, resized_images, image_downscale, positive_examples):
        '''
        Generates a negative example window and its position from the image layers
        
        Parameters
        ----------
        resized_images: list of arrays 
            resized layers of the original image
        image_downscale: floar
            downscale used to resize the image
        positive_examples: list [[i, j, height, width]]
            list of the positive labels
        
        Returns
        ----------
        window_image: array-like
            image corresponding to a negative face example 
        (i, j, ratio): tuple
            tuple containing the position of the window relative to the original image layer, and the ratio of the layer where the window was taken from
        '''

        overlap = True
        #We repeat while the negative window doesn't overlap with one of the positive labels
        while overlap:
            #randomly choose a layer
            layer_nbr = random.randint(0,len(resized_images)-1)
            resized_image = resized_images[layer_nbr]

            #Randomly choose the position of the window
            #the window needs to be far enough from the layer borders in order to have the required shape 
            img_height, img_width = resized_image.shape
            i = random.randint(0, img_height - self._window_shape[0])
            j = random.randint(0, img_width - self._window_shape[1])
            window_image = resized_image[i:i+self._window_shape[0], j:j+self._window_shape[1]]
            del resized_image
            
            #Calculate the position of the window relative to the original image layer.
            #This will be used to calculate the iou with the true labels
            ratio = np.power(image_downscale, layer_nbr) 
            original_window_params = [int(item * ratio) for item in [i, j, self._window_shape[0], self._window_shape[1]]]
            
            overlap = False
            #check if the window doesn't overlap with any of the true labels
            for window_params in positive_examples:
                if self._IoU(window_params, original_window_params) > self._iou_limit:
                    overlap = True
                    break

            if not overlap:
                return window_image, (i, j, ratio) 

# Face detection

The goal of this project is to perform a face detection on images, as shown in this example:
![](images/test_example.png)

## Training
A sliding window algorithm is applied, combined with a SVC (_Support Vector Classifier_) and HOG (_Histogram of Oriented Gradients_) features.

In order to train the model, train images must be added in the ```train/``` folder.  The file ```train.py``` can then be executed. The following command shows the possible arguments to change during the training process.
```python
python train.py -h
```
The best hyper-parameters, classification algorithm and feature descriptors were obtained using a cross-validation

A model is then generated in the file ```classifier.joblib```. In this case it was trained on 1000 images.

## Testing

This model can then be used for the prediction using the ```test.py``` file. The test images must be located in the ```test/``` folder. The following results were obtained on 500 test images:

- Precision:  99.07 %
- Recall   :  94.02 %
- F1-score :  96.48 %
- AUC      :  93.46 %

For more details, a report is written in ```Rapport/rapport.tex```.
'''
Train script
'''

import argparse
from face_detector import face_detector
from utils import load_data

parser = argparse.ArgumentParser()

parser.add_argument('--window_height', type=int , default=64,
                    help='height of the sliding window, must be the same for the train and test file  [default: 64]')
parser.add_argument('--window_width', type=int , default=64,
                    help='width of the sliding window, must be the same for the train and test file  [default: 64]')
parser.add_argument('--image_layers', type=int, default=10,
                    help='number of image layers to slide the window over [default: 10]')
parser.add_argument('--step', type=int, default=8,
                    help='step of the sliding window [default: 8]')
parser.add_argument('--train_path', type=str, default='train/',
                    help='path to the folder containing train images [default: train/ ]')
parser.add_argument('--label_path', type=str, default='label_train.txt',
                    help='path to the labels file [default: label_train.txt]')

FLAGS = parser.parse_args()

#Hyperparameters
WINDOW_SHAPE = (FLAGS.window_height, FLAGS.window_width)
IMAGE_LAYERS = FLAGS.image_layers
STEP = FLAGS.step
TRAIN_PATH = FLAGS.train_path
LABEL_PATH = FLAGS.label_path

X, y = load_data(TRAIN_PATH, LABEL_PATH)

detector = face_detector(window_shape=WINDOW_SHAPE, image_layers=IMAGE_LAYERS, step=STEP)
detector.fit(X, y, TRAIN_PATH)